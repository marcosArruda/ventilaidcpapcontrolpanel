# VentilAidCPAPControlPanel

User control panel software for VentilAid CPAP device

Main requirements:
1. Based on Qt5 or other framework that is supported on multiple platforms (should be able to run on Windows/Linux/MacOS, in later revisions we’d like it to be able to run on Raspberry or similar hardware with connected touchscreen),
2. Communication with hardware done by serial over USB, baudrate 115200 or greater. Software should be able to scan COM ports, show a list of available devices.
3. Refresh rate should be settable (it’s a setting that will be send to our device to set frequency of outgoing packets)
4. Software should be able to record received data to a file (it can be plain text or any other file type but it must be easily converted to graphs etc. for analysis)
5. Software should be able to send 5 setting values, as presented on the layout guideline screenshot. Values should be settable via sliders or by writing a value to window and hitting enter.
6. Software should be able to show received values in real-time manner on graph. It should be scalable with window size (bigger window - bigger graph) and clear.
7. If our hardware sends error frame, a new pop-up window should appear with error details
8. If our device doesn’t send any message for 5 sec - open error window and maybe sound an alarm?



